#include "bs_exam2_task2.h"

graph_node* create_node(unsigned num_inputs_needed, graph_node* target) {
	graph_node* ret = (graph_node*)malloc(sizeof(graph_node));
	ret->max = 0;
	ret->num_inputs_needed = num_inputs_needed;
	ret->target = target;
	return ret;
}

void destroy_node(graph_node* target) {
	free(target);
}

void push_input(graph_node* node, unsigned input) {
	node->current_input = input;
	perform_computation(node);
}

void perform_computation(graph_node* node) {
	if(node->current_input > node->max) {
		node->max = node->current_input;
	}
	node->num_inputs_needed--;
	if(node->num_inputs_needed == 0 && node->target != NULL) {
		push_input(node->target, node->max);
	}
}

void wait_for_node(graph_node* node) {
	// in sequential program we don't need to do anything here
}

